<?php
include("db.php");
$title = '';
$description= '';

if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM menus WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $title = $row['title'];
    $description = $row['description'];
    $id_position = $row['id_position'];
    $status = $row['status'];
    $id_mode = $row['id_mode'];
    $father_id = $row['father_id'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $title= $_POST['title'];
  $description = $_POST['description'];
  $id_position = $_POST['id_position'];
  $status = $_POST['status'];
  $id_mode = $_POST['id_mode'];
  $father_id = $_POST['father_id'];

  $query = "UPDATE menus set title = '$title', description = '$description', id_position = '$id_position', status = '$status', id_mode = '$id_mode', father_id = '$father_id' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Menus Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: index.php');
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="title" type="text" class="form-control" value="<?php echo $title; ?>" placeholder="Update Title">
        </div>
        <div class="form-group">
        <textarea name="description" class="form-control" cols="30" rows="10"><?php echo $description;?></textarea>
        </div>
        <div class="form-group">
            <select  class="form-control" name="id_position" id="id_position">
              <?php
                $sql=$conn->query("select * from positions");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id_position']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
          <div class="form-group">
            <input type="text" name="status" class="form-control" placeholder="Menus Status">
          </div>
          <div class="form-group">
            <select  class="form-control" name="id_mode" id="id_mode">
              <?php
                $sql=$conn->query("select * from modes");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id_mode']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
          <div class="form-group">
            <select  class="form-control" name="father_id" id="father_id">
              <?php
                $sql=$conn->query("select * from menus");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
        <button class="btn-success" name="update">
          Update
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
