<?php include("db.php"); ?>

<?php include('includes/header.php'); ?>

<main class="container p-4">
  <div class="row">
    <div class="col-md-4">
      <!-- MESSAGES -->

      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

      <!-- ADD Menus FORM -->
      <div class="card card-body">
        <form action="save_menus.php" method="POST">
          <div class="form-group">
            <input type="text" name="title" class="form-control" placeholder="Menus Title" autofocus>
          </div>
          <div class="form-group">
            <textarea name="description" rows="2" class="form-control" placeholder="Menus Description"></textarea>
          </div>
          <div class="form-group">
            <select  class="form-control" name="id_position" id="id_position">
              <option hidden>select a positions</option>
              <?php
                $sql=$conn->query("select * from positions");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id_position']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
          <div class="form-group">
            <input type="text" name="status" class="form-control" placeholder="Menus Status">
          </div>
          <div class="form-group">
            <select  class="form-control" name="id_mode" id="id_mode">
              <option hidden>select a mode</option>
              <?php
                $sql=$conn->query("select * from modes");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id_mode']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
          <div class="form-group">
            <select  class="form-control" name="father_id" id="father_id">
              <option hidden>select a menu</option>
              <?php
                $sql=$conn->query("select * from menus");

                while($fila=$sql->fetch_array()){
                    echo "<option value='".$fila['id']."'>".$fila['title']."</option>";
                }
              ?>            
            </select>            
          </div>
          <input type="submit" name="save_menus" class="btn btn-success btn-block" value="Save Menus">
        </form>
      </div>
    </div>
    <div class="col-md-8">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Title</th>
            <th>Description</th>
            <th>id_position</th>
            <th>status</th>
            <th>id_mode</th>
            <th>father_id</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM menus";
          $result_menus = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_menus)) { ?>
          <tr>
            <td><?php echo $row['title']; ?></td>
            <td><?php echo $row['description']; ?></td>
            <td><?php echo $row['id_position']; ?></td>
            <td><?php echo $row['status']; ?></td>
            <td><?php echo $row['id_mode']; ?></td>
            <td><?php echo $row['father_id']; ?></td>
            <td>
              <a href="edit.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="delete_menus.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
