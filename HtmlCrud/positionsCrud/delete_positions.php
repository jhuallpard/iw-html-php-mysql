<?php

include("db.php");

if(isset($_GET['id_position'])) {
  $id_position = $_GET['id_position'];
  $query = "DELETE FROM positions WHERE id_position = $id_position";
  $result = mysqli_query($conn, $query);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Positions Removed Successfully';
  $_SESSION['message_type'] = 'danger';
  header('Location: index.php');
}

?>
