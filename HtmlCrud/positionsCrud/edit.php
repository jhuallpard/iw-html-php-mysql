<?php
include("db.php");
$title = '';
$description= '';
$mode_id= '';

if  (isset($_GET['id_position'])) {
  $id_position = $_GET['id_position'];
  $query = "SELECT * FROM positions WHERE id_position=$id_position";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $title = $row['title'];
    $description = $row['description'];
    $mode_id = $row['mode_id'];
  }
}

if (isset($_POST['update'])) {
  $id_position = $_GET['id_position'];
  $title= $_POST['title'];
  $description = $_POST['description'];
  $mode_id = $_POST['mode_id'];

  $query = "UPDATE positions set title = '$title', description = '$description', mode_id = '$mode_id' WHERE id_position=$id_position";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Positions Updated Successfully';
  $_SESSION['message_type'] = 'warning';
  header('Location: index.php');
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="edit.php?id_position=<?php echo $_GET['id_position']; ?>" method="POST">
        <div class="form-group">
          <input name="title" type="text" class="form-control" value="<?php echo $title; ?>" placeholder="Update Title">
        </div>
        <div class="form-group">
        <textarea name="description" class="form-control" cols="30" rows="10"><?php echo $description;?></textarea>
        </div>
        <div class="form-group">
          <select  class="form-control" name="mode_id" id="mode_id">
            <?php
              $sql=$conn->query("select * from modes");

              while($fila=$sql->fetch_array()){
                echo "<option value='".$fila['id_mode']."'>".$fila['title']."</option>";
              }
            ?>            
          </select>            
        </div>
        <button class="btn-success" name="update">
          Update
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
