<?php

include("db.php");

if(isset($_GET['id_mode'])) {
  $id_mode = $_GET['id_mode'];
  $query = "DELETE FROM modes WHERE id_mode = $id_mode";
  $result = mysqli_query($conn, $query);
  if(!$result) {
    die("Query Failed.");
  }

  $_SESSION['message'] = 'Modes Removed Successfully';
  $_SESSION['message_type'] = 'danger';
  header('Location: index.php');
}

?>
