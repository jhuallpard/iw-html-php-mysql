<?php
include_once 'dbh.inc.php'
?>
    <?php
        //ARRAY DE MENUS
        $sql = "SELECT menus.title AS title , modes.description AS mode_style , positions.description AS pos
        FROM menus 
        JOIN modes ON menus.id_mode = modes.id_mode
        JOIN positions ON menus.id_position = positions.id_position
        WHERE menus.status = 1 ";
        $result = mysqli_query($conn,$sql);
        $menus = mysqli_fetch_all($result, MYSQLI_ASSOC);
        mysqli_free_result($result);
        //print_r($menus);

        //ARRAY DE MODOS
        /*
        $sql = "SELECT * FROM modes";
        $result = mysqli_query($conn,$sql);
        $resultCheck = mysqli_num_rows($result); modes.description AS mode_style , positions.description AS pos

        $modes = array();
        if($resultCheck > 0){
            while( $row = mysqli_fetch_assoc($result)){
                //echo $row['title'];
                array_push($modes,$row); 
            }
        }
        mysqli_free_result($result);
        //print_r($modes);

        //ARRAY DE POSITIONS
        $sql = "SELECT * FROM positions";
        $result = mysqli_query($conn,$sql);
        $resultCheck = mysqli_num_rows($result);

        $positions = array();
        if($resultCheck > 0){
            while( $row = mysqli_fetch_assoc($result)){
                //echo $row['title'];
                array_push($positions,$row); 
            }
        }
        mysqli_free_result($result);
        //print_r($positions);

        $pos_map = array("justify-self: start; align-self: flex-start;", "justify-self: end; align-self: flex-start;","justify-self: start; align-self: flex-end;","justify-self: end; align-self: flex-end;");*/

    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            Menus - Ingeniería Web
        </title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
        <style>
            html, body {
                        height: 100%;
                        }
        </style>
    </head>
    <body>
    <div class="container-fluid " style="min-height: 100%; height: 100%;">
        <?php foreach($menus as $menu ): ?>
            <div  style = "padding-left:5rem;" class = "<?php echo $menu['mode_style'] ?> p2 <?php echo $menu['pos'] ?>">
                <?php echo htmlspecialchars($menu['title']); ?>
            </div>
        <?php endforeach; ?>
    </div>
    </body>
</html>